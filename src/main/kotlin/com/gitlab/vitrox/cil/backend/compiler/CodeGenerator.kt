/*
 * compiler-interpreter
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.cil.backend.compiler

import com.gitlab.vitrox.cil.backend.Backend
import com.gitlab.vitrox.cil.intermediate.ICode
import com.gitlab.vitrox.cil.intermediate.SymTab
import com.gitlab.vitrox.cil.message.Message

class CodeGenerator : Backend() {

    var instructionCount = 0
        private set

    override fun process(iCode: ICode, symTab: SymTab) {

        measureTime {}

        sendMessage(Message.Summary.Compiler(this))
    }
}