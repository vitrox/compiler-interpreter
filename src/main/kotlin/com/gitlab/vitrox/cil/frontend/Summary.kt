/*
 * compiler-interpreter
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.cil.frontend

interface Summary {

    data class Time(var startTime: Long = 0,
                    var endTime: Long = 0)

    val time: Time

    private fun setStart() {
        time.startTime = System.currentTimeMillis()
    }

    private fun setEnd() {
        time.endTime = System.currentTimeMillis()
    }

    fun elapsedTime() = (time.endTime - time.startTime) / 1000

    fun <T> T.measureTime(lambda: T.() -> Unit) {
        setStart()
        lambda.invoke(this)
        setEnd()
    }

}