/*
 * compiler-interpreter
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.cil.frontend

import com.gitlab.vitrox.cil.frontend.reader.TextReader

abstract class Token(source: Source): TextReader by source {

    // TODO: lateinit not nullable
    var type: TokenType? = null
        protected set

    var text: String = currentChar().toString()
        protected set

    var value: Any? = null
        protected set

    open fun extract() {
        text = currentChar().toString()
        value = null
        nextChar()
    }

    final override fun currentChar(): Char?
            = super.currentChar()

}