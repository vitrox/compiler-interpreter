/*
 * compiler-interpreter
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.cil.frontend.reader

/**
 * Null when [line] is null
 * CHAR_NULL when [currentPos] is invalid for length of [line]
 */
interface LineReader {

    companion object {
        const val CHAR_NULL = '\u0000'
    }

    var line: String?

    var currentPos: Int

    fun currentChar(): Char?
            = line?.let {
        if (it.isEmpty()) CHAR_NULL
        else it[currentPos]
    }

    fun nextChar(): Char?
        = peekChar()?.also { if (it != CHAR_NULL) currentPos++ }

    fun peekChar(): Char?
            = line?.let {
        if (hasNext()) it[currentPos+1]
        else CHAR_NULL
    }

    fun hasNext(): Boolean
        = line?.let { currentPos < it.length-1 } ?: false

}