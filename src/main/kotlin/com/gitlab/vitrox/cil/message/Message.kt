/*
 * compiler-interpreter
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.cil.message

import com.gitlab.vitrox.cil.backend.compiler.CodeGenerator
import com.gitlab.vitrox.cil.backend.interpreter.Executor
import com.gitlab.vitrox.cil.frontend.TokenType

// TODO: Remove hardcoded part, and make message classes ready for change (maybe remove data class => only class)
sealed class Message {

    data class SourceLine(val lineNum: Int,
                          val line: String) : Message() {

        override val format by lazy { "${lineNum.toString().padStart(3, '0')} $line" }
    }

    data class SyntaxError(val lineNum: Int,
                           val position: Int,
                           val text: String?,
                           val errorMessage: String) : Message() {

        constructor(token: com.gitlab.vitrox.cil.frontend.Token, errorCode: TokenType) :
                this(token.lineNumber(),
                        token.currentPos,
                        token.text,
                        errorCode.toString())

        companion object {
            const val PREFIX_WIDTH = 5
        }

        override val format by lazy {
            "${" ".repeat(spaceCount)}${"^n*** $errorMessage"}".apply {
                if (text != null) this.plus(" [at \"$text\"]")
            }
        }

        val spaceCount by lazy { PREFIX_WIDTH + position }

    }

    sealed class Summary : Message() {

        data class Parser(val lineNum: Int,
                          val errorCount: Int,
                          override val elapsedTime: Long) : Summary() {

            constructor(parser: com.gitlab.vitrox.cil.frontend.Parser) :
                    this(parser.currentPos,
                            parser.getErrorCount(),
                            parser.elapsedTime())

            constructor(lineNum: Int, errorCount: Int, summary: com.gitlab.vitrox.cil.frontend.Summary) :
                    this(lineNum, errorCount, summary.elapsedTime())

            override val format by lazy {
                "$lineNum source lines.\n" +
                        "$errorCount syntax errors.\n" +
                        "$elapsedTime seconds total parsing time."
            }

        }

        data class Interpreter(val executionCount: Int,
                               val runtimeErrors: Int,
                               override val elapsedTime: Long) : Summary() {

            constructor(executor: Executor) :
                    this(executor.executionCount,
                            executor.runtimeErrors,
                            executor.elapsedTime())

            constructor(executionCount: Int, runtimeErrors: Int, summary: com.gitlab.vitrox.cil.frontend.Summary) :
                    this(executionCount, runtimeErrors, summary.elapsedTime())

            override val format by lazy {
                "$executionCount statements executed.\n" +
                        "$runtimeErrors runtime errors.\n" +
                        "$elapsedTime seconds total execution time."
            }

        }

        data class Compiler(val instructionCount: Int,
                            override val elapsedTime: Long) : Summary() {

            constructor(codeGenerator: CodeGenerator) :
                    this(codeGenerator.instructionCount,
                            codeGenerator.elapsedTime())

            constructor(instructionCount: Int, summary: com.gitlab.vitrox.cil.frontend.Summary) :
                    this(instructionCount, summary.elapsedTime())

            override val format by lazy {
                "$instructionCount instructions generated.\n" +
                        "$elapsedTime seconds total code generation time."
            }

        }

        abstract val elapsedTime: Long
        override val format by lazy { "$elapsedTime seconds total elapsed time." }

    }

    //data class MISCELLANEOUS(): Message()

    data class Token(val lineNum: Int,
                     val position: Int,
                     val type: TokenType?,
                     val text: String,
                     val value: Any?) : Message() {

        constructor(token: com.gitlab.vitrox.cil.frontend.Token) :
                this(token.lineNumber(),
                        token.currentPos,
                        token.type,
                        token.text,
                        token.value)

        override val format by lazy { ">>> $type line=$lineNum, pos=$position, text=$text" }

        val formatValue by lazy { ">>> value='\'${value.toString()}'\'" }

        fun printValue() {
            println(formatValue)
        }

    }

    /*data class ASSIGN(): Message()

    data class FETCH(): Message()

    data class BREAKPOINT(): Message()

    data class RUNTIME_ERROR(): Message()

    data class CALL(): Message()

    data class RETURN(): Message()*/

    abstract val format: String

    fun print() {
        println(format)
    }

}